package test;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class InstagramRobot2 extends JFrame {

	static JLabel status = new JLabel("正在运行");
	static int deltaY = 21;
	static int deltaX = 335;
	static int pointerY = 114 - deltaY;
	static int pointerX = 324;
	static int col_nth = 1;
	static Random ran;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InstagramRobot2 display = new InstagramRobot2();

		display.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
			}
		});

		try {
			Robot myRobot = new Robot();
			myRobot.delay(2500); // 此时间内赶紧输入焦点转到浏览器

			Timer timer = new Timer();
			ran = new Random(); // 随机延迟
			timer.schedule(new TimerTask() { // timer开启一个新线程
				@Override
				public void run() { // 主要过程

					myRobot.mouseMove(384, 948);

					singleClickLeft(myRobot);

					myRobot.delay(1500 + ran.nextInt(1000));

					// 进位--判断--执行
					pointerY += deltaY;
					if (pointerY > 925) {
						if (col_nth < 5) {
							col_nth++;
							pointerX += deltaX;
							pointerY = 114;
						} else {
							status.setText("--任务完成--");
							timer.cancel();
						}

					}
					myRobot.mouseMove(pointerX, pointerY);

					doubleClickLeft(myRobot);

					myRobot.delay(1500 + ran.nextInt(1000));

					myRobot.mouseMove(538, 179);

					singleClickLeft(myRobot);

					myRobot.delay(1500 + ran.nextInt(1000));

					singleClickLeft(myRobot);

					myRobot.delay(7500 + ran.nextInt(1000));

				}
			}, 100, 10000);

		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void singleClickLeft(Robot myRobot) {
		myRobot.mousePress(KeyEvent.BUTTON1_DOWN_MASK); // 模拟按下鼠标左键
		myRobot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
	}

	public static void doubleClickLeft(Robot myRobot) {
		myRobot.mousePress(KeyEvent.BUTTON1_DOWN_MASK); // 模拟按下鼠标左键
		myRobot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
		myRobot.mousePress(KeyEvent.BUTTON1_DOWN_MASK); // 模拟按下鼠标左键
		myRobot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
	}

	public InstagramRobot2() {
		setTitle("Ins机器人");
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

		setBounds((int) (screenSize.getWidth() - 256), 100, 200, 128);
		setAlwaysOnTop(true);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(status, BorderLayout.CENTER);
		setVisible(true);
	}

}
