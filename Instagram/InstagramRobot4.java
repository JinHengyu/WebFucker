package test;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class InstagramRobot4 extends JFrame {

	/**
	 * @author Jim 2195868682@qq.com
	 */
	private static final long serialVersionUID = 1L;
	static JLabel status = new JLabel("正在运行(esc终止)");
	static int deltaX = 335; // x跨度(像素)
	static int deltaY = 21; // y跨度(像素)
	static int rows = 39; // 行数
	static int cols = 5; // 列数
	static int startX = 324; // 初始x坐标
	static int startY = 114; // 初始y坐标
	static int x = startX; // 变量x坐标
	static int y = startY; // 变量y坐标
	static Random ran; // 用于生成随机时间间隔,欺骗Ins

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InstagramRobot4 display = new InstagramRobot4();

		display.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
					System.exit(0);
				}
			}
		});

		try {
			Robot myRobot = new Robot();
			myRobot.delay(2500); // 此时间内赶紧输入焦点转到浏览器

			ran = new Random(); // 随机延迟

			// 二维遍历文件上传控件中的所有文件
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					x = startX + j * deltaX;
					y = startY + i * deltaY;

					myRobot.mouseMove(384, 948);

					singleClickLeft(myRobot);

					myRobot.delay(1500 + ran.nextInt(1000));

					myRobot.mouseMove(x, y);

					doubleClickLeft(myRobot);

					myRobot.delay(1500 + ran.nextInt(1000));

					myRobot.mouseMove(538, 179);

					singleClickLeft(myRobot);

					myRobot.delay(1000 + ran.nextInt(1000));

					singleClickLeft(myRobot);

					myRobot.delay(7500 + ran.nextInt(1000));
				}
			}

			status.setText("--任务完成--");

		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void singleClickLeft(Robot myRobot) {
		myRobot.mousePress(KeyEvent.BUTTON1_DOWN_MASK);
		myRobot.mouseRelease(KeyEvent.BUTTON1_DOWN_MASK);
	}

	public static void doubleClickLeft(Robot myRobot) {
		singleClickLeft(myRobot);
		singleClickLeft(myRobot);
	}

	public InstagramRobot4() {
		setTitle("Ins机器人");
		Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

		setBounds((int) (screenSize.getWidth() - 256), (int) (screenSize.getHeight() - 128), 200, 100);
		setAlwaysOnTop(true);
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(status, BorderLayout.CENTER);
		setVisible(true);
	}

}
