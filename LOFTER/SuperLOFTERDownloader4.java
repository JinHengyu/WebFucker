package test;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SuperLOFTERDownloader4 {
	static int count = 0;
	static String username = "shendingding";
	static String url = "http://" + username + ".lofter.com/view";
	static String picUrl;
	static ArrayList<String> urlList = new ArrayList<String>();

	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\Jim\\Desktop\\GeckoDriver\\geckodriver.exe");

		/*
		 * FirefoxBinary firefoxBinary = new FirefoxBinary();
		 * firefoxBinary.addCommandLineOptions("--headless"); FirefoxOptions
		 * firefoxOptions = new FirefoxOptions();
		 * firefoxOptions.setBinary(firefoxBinary); FirefoxDriver driver = new
		 * FirefoxDriver(firefoxOptions);
		 */

		FirefoxOptions options = new FirefoxOptions();
		options.addPreference("permissions.default.image", 2);
		FirefoxDriver driver = new FirefoxDriver(options);

		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); // 隐式等待
		driver.get(url);

		// ((JavascriptExecutor)
		// driver).executeScript("document.querySelector('.m-txtsch').style.display=\"inline\"");

		int number = Integer.parseInt(driver.findElementByCssSelector(
				"body > div.g-bdfull.g-bdfull-show.ztag > div.g-bdc.ztag > div.m-fbar.f-cb > div.schbtn.f-cb > div:nth-child(1) > div > div.txt > a.ztag.currt > span")
				.getAttribute("innerHTML"));

		Actions action = new Actions(driver);

		WebDriverWait wait = new WebDriverWait(driver, 12, 256);

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				action.sendKeys(/* driver.findElement(By.cssSelector("body")), */ Keys.END).perform();
			}
		}, 1, 500);

		wait.until(ExpectedConditions
				.numberOfElementsToBe(By.cssSelector("div.ztag > div.m-filecnt.m-filecnt-1 > ul > li"), number));
		timer.cancel();

		for (WebElement element : driver.findElements(
				By.cssSelector("div.ztag > div.m-filecnt.m-filecnt-1 > ul > li > a > div > div > img.realimg"))) {
			picUrl = element.getAttribute("src").split("\\?")[0];
			System.out.println(picUrl);
			urlList.add(picUrl);
			count++;

		}

		System.out.println("爬取到" + number + "篇文章中的" + count + "张图的url");

		int flag = JOptionPane.showConfirmDialog(null, "核对url,是否开始下载?", "是否继续", JOptionPane.YES_NO_OPTION);
		if (flag == JOptionPane.NO_OPTION) {
			System.exit(0);
		} else if (flag == JOptionPane.YES_OPTION) {
			System.out.println("正在下载.....");
			System.out.println("总共下载了" + DownloadFromUrlList.download(urlList, "E:\\爬虫\\美食\\") + "张图");
		}
		// JOptionPane.showMessageDialog(null, "浏览器可以关闭了吗?");
		driver.quit();
	}
}