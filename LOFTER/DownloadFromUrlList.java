package test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class DownloadFromUrlList {
	static File file;
	static int count = 0;
	static URL url;
	static String extention;
	static InputStream is;
	static OutputStream os;
	static byte[] buffer = new byte[1024];
	static int len;

	public static int download(ArrayList<String> urlList, String dirPath) {
		if (new File(dirPath).exists()) {
			if (!new File(dirPath).isDirectory()) {
				System.out.println("ERROR!! THE PATH GIVEN ISN'T A DIRECTORY");
				return 0;
			}
		} else {
			new File(dirPath).mkdir();
		}

		try {
			for (String urlstr : urlList) {
				count++;
				url = new URL(urlstr);
				is = new BufferedInputStream(url.openStream());
				extention = "." + HttpURLConnection.guessContentTypeFromStream(is).split("/")[1];
				file = new File(dirPath + count + extention);
				os = new FileOutputStream(file);
				while ((len = is.read(buffer)) != -1) {
					os.write(buffer, 0, len);
				}
				is.close();
				os.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return count;
	}

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		list.add("http://img.taopic.com/uploads/allimg/130226/234592-1302261U10533.jpg");
		list.add("http://img3.redocn.com/tupian/20150318/qingxinshuzhibiankuang_4021000.jpg");
		download(list, "C:\\Users\\Jim\\Desktop\\");
	}

}
